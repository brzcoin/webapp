import React from 'react'
import './styles/App.scss';

import Menu from './components/Menu'
import PanelController from './components/Panels/PanelController'

import MenuContext from './contexts/MenuContext'

function App() {
  const [menu, setMenu] = React.useState('home')
  const value = { menu, setMenu }
  
  return (
    <MenuContext.Provider value={value}>
      <div className="App">
        <Menu />
        <PanelController />
      </div>
    </MenuContext.Provider>
  );
}

export default App;
