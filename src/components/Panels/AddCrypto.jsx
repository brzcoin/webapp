import React from 'react'
import '../../styles/AddCrypto.scss'

function AddCrypto() {
    return (
        <section className="AddCrypto-wrapper">
            <form id="addCoin">
                <select name="Coin" id="ccoin" form="addCoin">
                    <option value="btc">BTC</option>
                    <option value="ada">ADA</option>
                    <option value="eth">ETH</option>
                    <option value="iota">IOTA</option>
                </select>
                <input
                    type="number"
                    placeholder="valor coin comprada"
                />
                <input 
                    type="number"
                    placeholder="valor que pagou"
                />
            </form>
        </section>
    )
}

export default AddCrypto
