import React from 'react'
import '../../styles/ListCoins.scss'
import '../../styles/CoinBox.scss'

const API_URI = "https://api.coingecko.com/api/v3"
const API_GETALL = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=brl&ids=ethereum%2Cbitcoin%2Ciota%2Cdogecoin%2Cripple%2Cchainlink&order=market_cap_desc&per_page=100&page=1&sparkline=false&price_change_percentage=24h"
const coinsList = [
    'bitcoin',
    'ethereum',
    'ripple',
    'cardano',
    'iota'
]

const CoinBox = ({ coin }) => {
    return (
        <div className="CoinBox-wrapper">
            {coin.id}
        </div>
    )

}

const ListCoins = () => {
    const [coinsInfo, setCoinsInfo] = React.useState([])

    React.useEffect(() => {
        fetch(API_GETALL)
            .then(response => response.json())
            .then(data => setCoinsInfo(data))
    }, [])


    console.log(coinsInfo)
    return (
        <section className="ListCoins-wrapper">
            <div className="ListCoins-top">
                {coinsInfo.map(coin => {
                    return (
                        <CoinBox coin={coin} key={coin.id} />
                    )
                })}
            </div>
            <div className="ListCoins-bottom">

            </div>
        </section>
    )
}

export default ListCoins
