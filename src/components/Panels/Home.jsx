import React from 'react'
import '../../styles/Home.scss'

function Home() {
    return (
        <section className="Home-wrapper">
            <div className="Home-left">
                <div className="Home-l-boxes">
                    <h2>Welcome back, Matheus Toscano</h2>
                    <div className="Home-l-container-box">
                        <div className="Home-l-box" />
                        <div className="Home-l-box" />
                        <div className="Home-l-box" />
                    </div>
                </div>
                <div className="Home-l-container-news">
                    <h2>Last news</h2>
                </div>
            </div>
            <div className="Home-right">
                <div className="Home-r-portfolio-value">
                    <p>My portfolio</p>
                    <h4>$ 2.820,00</h4>
                </div>
                <div className="Home-r-portfolio-coins">
                    <div className="Home-r-high">
                        <div>
                            <p className="Home-r-portfolio-coinname">BTC</p>
                            <span>+21%</span>
                        </div>
                        <h4>R$ 310.200,19</h4>
                    </div>
                    <div className="Home-r-low">
                        <div>
                            <p className="Home-r-portfolio-coinname">XRP</p>
                            <span>-6%</span>
                        </div>
                        <h4>R$ 2,87</h4>
                    </div>
                </div>
                <div></div>
            </div>
        </section>
    )
}

export default Home
