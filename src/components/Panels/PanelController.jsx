import React from 'react'
import '../../styles/PanelController.scss'
import Home from '../Panels/Home'
import ListCoins from '../Panels/ListCoins'
import AddCrypto from '../Panels/AddCrypto'
import Portfolio from '../Panels/Portfolio'


import MenuContext from '../../contexts/MenuContext'

function PanelController() {
    const menuSelection = React.useContext(MenuContext)
    
    console.log(menuSelection)
    
    const renderPanel = (value) => {
        switch (value) {
            case 'home':
                return <Home />
            case 'portfolio':
                return <Portfolio />
            case 'coins':
                return <ListCoins />
            case 'profile':
                return <AddCrypto />
            default:
                return <Home />
        }
    }

    return (
        <section className="PanelController-wrapper">
            {renderPanel(menuSelection.menu)}
        </section>
    )
}

export default PanelController
