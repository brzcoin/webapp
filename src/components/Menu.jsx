import React from 'react'
import '../styles/Menu.scss'
import { RiHome3Fill } from 'react-icons/ri'
import { FaWallet, FaCoins, FaMoneyCheckAlt } from 'react-icons/fa'

import MenuContext from '../contexts/MenuContext'

function Menu() {
    const {menu, setMenu} = React.useContext(MenuContext)

    return (
            <section className="Menu-wrapper">
                <div className="Menu-container">
                    <div className="Menu-logo">brz</div>
                    <div className="Menu-options">
                        <div style={menu === 'home' ? { borderRight: '3px solid #2ec4b6' } : {}} onClick={() => setMenu('home')}>
                            <RiHome3Fill />
                        </div>
                        <div style={menu === 'portfolio' ? { borderRight: '3px solid #2ec4b6' } : {}} onClick={() => setMenu('portfolio')}>
                            <FaWallet />
                        </div>
                        <div style={menu === 'coins' ? { borderRight: '3px solid #2ec4b6' } : {}} onClick={() => setMenu('coins')}>
                            <FaCoins />
                        </div>
                        <div style={menu === 'profile' ? { borderRight: '3px solid #2ec4b6' } : {}} onClick={() => setMenu('profile')}>
                            <FaMoneyCheckAlt />
                        </div>
                    </div>
                    <div style={{ color: 'white' }}>&copy;</div>
                </div>
            </section>
    )
}

export default Menu
