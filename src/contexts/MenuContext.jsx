import React from 'react';

const MenuContext = React.createContext({
    menu: 'home',
    setMenu: () => {}
})

export default MenuContext;